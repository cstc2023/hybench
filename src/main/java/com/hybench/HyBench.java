package com.hybench;
/**
 *
 * @time 2023-03-04
 * @version 1.0.0
 * @file Hybench.java
 * @description
 *      Here is the main class. Load configuration from conf file and read options from command line.
 *      Four different test types are provided, including runAP, runTP, runXP ,runHTAP and runAll.
 **/

import com.hybench.dbconn.ConnectionMgr;
import com.hybench.load.DataGenerator;
import com.hybench.load.DataImportFactory;
import com.hybench.load.ExecSQL;
import com.hybench.stats.Result;
import com.hybench.workload.Client;
import com.hybench.workload.Freshness;
import com.hybench.workload.SqlReader;
import com.hybench.workload.Sqlstmts;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class HyBench {
    public static Logger logger = LogManager.getLogger();
    int taskType = 0;
    Result res = new Result();
    boolean verbose = true;
    Sqlstmts sqls = null;

    public Result getRes(){
        return res;
    }

    void setSqls(Sqlstmts sqlloader) {
        this.sqls = sqlloader;
    }

    Sqlstmts getSqls(){
        return sqls;
    }

    // run TP type workload. Spouse nums of threads defined in conf file.
    public void runTP(){
        logger.info("Begin TP Workload");
        taskType = 1;
        String tpClient = ConfigLoader.prop.getProperty("tpclient");

        List<Client> tasks = new ArrayList<Client>();
        if(Integer.parseInt(tpClient) > 0){
            Client job = Client.initTask(ConfigLoader.prop,"TPClient",taskType,sqls);
            job.setRet(res);
            job.setVerbose(verbose);
            tasks.add(job);
        }
	    else {
            logger.warn("There is no an available tp client");
            return;
        }
        ExecutorService es = Executors.newFixedThreadPool(tasks.size());
        List<Future> future = new ArrayList<Future>();
        for (final Client j : tasks) {
            future.add( es.submit(new Runnable() {
                    public void run() {
                    j.startTask();
                }
                })
            );
        }
        for(int flength=0;flength < future.size();flength++) {
            Future f = future.get(flength);
            if (f != null && !f.isCancelled() && !f.isDone()) {
                try {
                    f.get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        }

        if (!es.isShutdown() || !es.isTerminated()) {
            es.shutdownNow();
        }

        logger.info("TP Workload is done.");
    }

    public void runAPower(){
        logger.info("Begin AP Workload");
        taskType = 2;
        String apClient = "1";
        Client job = Client.initTask(ConfigLoader.prop,"APClient",taskType,sqls);
        job.setRet(res);
        job.setVerbose(verbose);
        ExecutorService es = Executors.newFixedThreadPool(1);
        Future future = es.submit(new Runnable() {
            public void run() {
                // TODO Auto-generated method stub
                job.startTask();
            }}
        );

        if (future != null && !future.isCancelled() && !future.isDone()) {
            try {
                future.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }

        if (!es.isShutdown() || !es.isTerminated()) {
            es.shutdownNow();
        }

        logger.info("AP Workload is done.");
    }

    public void runAP(){
        logger.info("Begin AP Workload");
        taskType = 7;

        String apClient = ConfigLoader.prop.getProperty("apclient");

        Client job = Client.initTask(ConfigLoader.prop,"APClient",taskType,sqls);
        job.setRet(res);
        job.setVerbose(verbose);
        ExecutorService es = Executors.newFixedThreadPool(1);
        Future future = es.submit(new Runnable() {
            public void run() {
                // TODO Auto-generated method stub
                job.startTask();
            }}
        );

        if (future != null && !future.isCancelled() && !future.isDone()) {
            try {
                future.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }

        if (!es.isShutdown() || !es.isTerminated()) {
            es.shutdownNow();
        }

        logger.info("AP Workload is done.");
    }

    public void runXP(int tt){
        logger.info("Begin XP Workload");
        taskType = tt;

        String tpClient = ConfigLoader.prop.getProperty("xtpclient","-1");
        if(tpClient.equalsIgnoreCase("-1")) {
            logger.error("Missing configuration xtpclient");
            System.exit(-1);
        }
        String apClient = ConfigLoader.prop.getProperty("xapclient","-1");
        if(tpClient.equalsIgnoreCase("-1")) {
            logger.error("Missing configuration xapclient");
            System.exit(-1);
        }

        List<Client> tasks = new ArrayList<Client>();
        if(Integer.parseInt(tpClient) > 0){
            Client job = Client.initTask(ConfigLoader.prop,"TPClient",taskType,sqls);
            job.setRet(res);
            job.setVerbose(verbose);
            tasks.add(job);
        }

        if(Integer.parseInt(apClient) > 0){
            Client job = Client.initTask(ConfigLoader.prop,"APClient",taskType,sqls);
            job.setRet(res);
            job.setVerbose(verbose);
            tasks.add(job);
        }

        ExecutorService es = Executors.newFixedThreadPool(tasks.size());
        List<Future> future = new ArrayList<Future>();
        for (final Client j : tasks) {
            future.add(es.submit(new Runnable() {
                public void run() {
                    j.startTask();
                }})
            );
        }
        Thread freshness = null;
        if(taskType == 4){
            final Timestamp startTs = new Timestamp(System.currentTimeMillis());
            final int _duration = Integer.parseInt(ConfigLoader.prop.getProperty("xpRunMins"));
            final int _fresh_interval = Integer.parseInt(ConfigLoader.prop.getProperty("fresh_interval",String.valueOf(20)));
            String db = ConfigLoader.prop.getProperty("db");
            int dbType = Constant.getDbType(db);
            freshness = new Thread(){

                public void run() {
                    Connection conn_tp = ConnectionMgr.getConnection(0);
                    Connection conn_ap = ConnectionMgr.getConnection(1);
                    double sum = 0;
                    int cnt = 0;
                    long duration = _duration * 60 * 1000L;
                    long elpased_time = 0L;
                    boolean hasGetFreshness = false;
                    for (int i = 0; i < _fresh_interval; i++) {
                        try {
                            Thread.sleep(_duration * 60 * 1000/_fresh_interval);
                            elpased_time += _duration * 60 * 1000/_fresh_interval;
                            Freshness fresh = new Freshness(dbType,conn_tp,conn_ap,sqls,startTs);
                            if(fresh.calcFreshness() == 2147483647){
                                continue;
                            }
                            sum += fresh.calcFreshness();
                            cnt++;
                            res.setFresh(sum/cnt * 1.0);
                            hasGetFreshness=true;
                        } catch (InterruptedException e) {
                            logger.info("Freshness checker was stopped in force");
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                    if(!hasGetFreshness){
                        res.setFresh(2147483647);
                    }
                    try {
                        if(conn_ap != null) {
                            conn_ap.close();
                        }
                        if(conn_tp != null) {
                            conn_tp.close();
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            };
            freshness.start();
        }

        for(int flength=0;flength < future.size();flength++) {
            Future f = future.get(flength);
            if (f != null && !f.isCancelled() && !f.isDone()) {
                try {
                    f.get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        }

        if(freshness != null){
            freshness.interrupt();
        }

        if (!es.isShutdown() || !es.isTerminated()) {
            es.shutdownNow();
        }
    
        logger.info("XP Workload is done");
    }

    public void runFreshness(int tt){

        logger.info("Begin Freshness Workload");
        runXP(tt);
        logger.info("Freshness Workload is done.");
    }
    
    public static void doDataImport(Map<String, String> tableMaps) throws SQLException {
        // add loading code for the target DB here
        try(Connection conn = ConnectionMgr.getConnection()) {
            String defaultDb = ConfigLoader.prop.getProperty("db");
            DataImportFactory.loadData(tableMaps, conn, ConfigLoader.prop.getProperty("auto_loader", defaultDb));
        } catch (Exception exp) {
            if (exp instanceof SQLException) {
                throw (SQLException) exp;
            } else {
                exp.printStackTrace();
                throw new SQLException(exp.getMessage());
            }
        }
    }

    public static void main(String[] args) throws SQLException {
        LoggerContext context = (LoggerContext) LogManager.getContext(false);
        File file = new File("conf/log4j2.properties");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        context.setConfigLocation(file.toURI());
        ConfigLoader config = new ConfigLoader();
        HyBench hybench = new HyBench();
        System.out.println("=============================================");
        System.out.println("|  _    _       ____                  _      |");
        System.out.println("| | |  | |     |  _ \\                | |     |");
        System.out.println("| | |__| |_   _| |_) | ___ _ __   ___| |__   |");
        System.out.println("| |  __  | | | |  _ < / _ \\ '_ \\ / __| '_ \\  |");
        System.out.println("| | |  | | |_| | |_) |  __/ | | | (__| | | | |");
        System.out.println("| |_|  |_|\\__, |____/ \\___|_| |_|\\___|_| |_| |");
        System.out.println("|          __/ |                             |");
        System.out.println("|         |___/                              |");
        System.out.println("=============================================");

        logger.info("Hi~Bench, HyBench");
        CommandProcessor cmdProcessor = new CommandProcessor(args);
        HashMap<String,String> argsList = cmdProcessor.commandPaser(args);
        int type = 0;
        String cmd = null;
        if(!argsList.containsKey("t") || !argsList.containsKey("c")){
            logger.error("Missing required options -t or -c,please check");
            cmdProcessor.printHelp();
            System.exit(-1);
        }

        if(argsList.containsKey("c")){
            ConfigLoader.confFile = argsList.get("c");
            config.loadConfig();
            config.printConfig();
        }

        if(argsList.containsKey("s")){
            hybench.verbose = false;
        }

        if(argsList.containsKey("t")){
            cmd = argsList.get("t");
            if(cmd.equalsIgnoreCase("sql")){
                if(argsList.containsKey("f")){
                    ExecSQL execSQL = new ExecSQL(argsList.get("f"));
                    execSQL.execute();
                }
                else{
                    logger.error("Maybe missing sql file argument -f ,please try to use help to check usage.");
                }
            }
            else if(cmd.equalsIgnoreCase("gendata")){
                if(Integer.parseInt(ConfigLoader.prop.getProperty("genData_Threads","1")) < 1){
                    System.out.println("The num of thread to generate data can't be less than 1 ");
                    System.exit(-1);
                }
                DataGenerator new_dg = new DataGenerator(
                        ConfigLoader.prop.getProperty("sf"),
                        ConfigLoader.getBoolean("force_data", true),
                        ConfigLoader.prop.getProperty("force_load_path", null));
                new_dg.dataGenerator();
               // add loading code for the target DB here
            //    doDataImport(new_dg.getTableName2DataFileMap());
            }
            else if(cmd.startsWith("run")) {
                String sqlsPath = argsList.get("f");
                SqlReader sqlStmt = new SqlReader(sqlsPath);
                hybench.setSqls(sqlStmt.loader());
                hybench.getRes().setStartTS(dateFormat.format(new Date()));
                if(cmd.equalsIgnoreCase("runxp") ){
                    type=0;
                    hybench.runXP(0);
                }
                else if(cmd.equalsIgnoreCase("runtp")){
                    type=1;
                    hybench.runTP();
                }
                else if(cmd.equalsIgnoreCase("runap")){
                    type=7;
                    hybench.runAP();
                }
                else if(cmd.equalsIgnoreCase("runappower")){
                    type=2;
                    hybench.runAPower();
                }
                else if(cmd.equalsIgnoreCase("runhtap")){
                    type=3;
                    hybench.runAP();
                    hybench.runTP();
                    hybench.runXP(0);
                }
                else if(cmd.equalsIgnoreCase("runFresh")){
                    type=4;
                    hybench.runFreshness(4);
                }
                else if(cmd.equalsIgnoreCase("runAll")){
                    type=6;
                    hybench.runAP();
                    hybench.runTP();
                    hybench.runFreshness(4);
                }
                else{
                    logger.error("Run task not found : " + cmd);
                    cmdProcessor.printHelp();
                    System.exit(-1);
                }
                hybench.getRes().setEndTs(dateFormat.format(new Date()));
                logger.info("Congs~ Test is done! Bye!");
                hybench.getRes().printResult(type);
            }
            else{
                logger.error("Not a known test type,please check");
                cmdProcessor.printHelp();
                System.exit(-1);
            }

        }
        else{
            cmdProcessor.printHelp();
            System.exit(-1);
        }
    }

}
